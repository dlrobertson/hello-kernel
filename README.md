# Small, "Hello, World!" kernels written in [LLVM](http://llvm.org/docs/LangRef.html) and [Rust](https://www.rust-lang.org/)


```sh
make llvm # build the kernel written in LLVM

make rust # build the kernel written in Rust

make run # run the kernel previously built with qemu
```